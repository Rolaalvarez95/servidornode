const mongoose = require('mongoose');
const CATEGORIA = require('../models/categoriaModels');


async function getCategorias() {

    let data = await CATEGORIA.find();
    return data;
}


module.exports = {
    getCategorias
}