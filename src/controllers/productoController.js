const mongoose = require('mongoose');
const PRODUCTO = require('../models/productoModels');


async function getProductos() {

    let data = await PRODUCTO.find();
    return data;
}
async function getProducto(id) {

    let data = await PRODUCTO.findById({_id:id});
    return data;
}

module.exports = {
    getProductos,
    getProducto
}