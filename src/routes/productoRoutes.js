const express = require("express");
const app = express();
const PRODUCTOS = require('../controllers/productoController');
const CATEGORIA =require('../controllers/categoriaController')

async function getProductos(req,res){
    try {
        const productos = await PRODUCTOS.getProductos();
        res.json(productos)
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un Error.')
    }
}
async function getProducto (req,res){
   
        let producto = await PRODUCTOS.getProducto(req.params.id)
        if(!producto){
            res.status(404).send({msg:'No Existe el producto'});
        }  
        res.json(producto)
   
}
async function getCategorias (req,res){
    try {
        const categorias = await CATEGORIA.getCategorias();
        res.json(categorias)
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un Error.')
    }

}
app.get('/productos', getProductos)
app.get('/producto/:id',getProducto)
app.get('/categorias',getCategorias)

module.exports = app