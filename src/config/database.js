require('./config');
const Mongoose = require('mongoose').Mongoose;

const dbProductos = new Mongoose();

dbProductos.connect(process.env.urlMongo, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
}).then(resp => {
    console.log('CONEXION A MONGO ESTABLECIDA ');
})

module.exports = {dbProductos};