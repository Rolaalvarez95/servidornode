const { dbProductos } = require("../config/database");
const Schema = dbProductos.Schema;
const CategoriaSchema = new Schema({
  NOMBRE: { type: String },
});

module.exports = dbProductos.model("Categorias", CategoriaSchema, "CATEGORIAS");
