const { dbProductos } = require("../config/database");
const Schema = dbProductos.Schema;
const ProductoSchema = new Schema({
  CODIGO: { type: String },
  NOMBRE: { type: String },
  UNITID: { type: String },
  PRECIO_VENTA: { type: Number },
  COSTO_FINANCIERO: { type: String },
  IDLINEA: { type: String },
  IDCATEGORIA: { type: String },
  CODIDPROVEEDOR: { type: String },
  ESTADO: { type: String },
  TIPOFABRICANTE: { type: String },
  NUMERO_PARTE: { type: String },
  LINEA: { type: String },
  CATEGORIA: { type: String },
  UEN: { type: String },
  MARCA: { type: String },
  PRECIO_MINIMO: { type: Number },
  DESCRIPCION: { type: String },
  IMAGEN_150: { type: String },
  IMAGEN_450: { type: String },
});

module.exports = dbProductos.model("Productos", ProductoSchema, "PRODUCTOS");
