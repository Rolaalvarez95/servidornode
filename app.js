require('./src/config/config')
const express = require("express");

const app = express();

const bodyParser = require("body-parser");
const cors = require("cors");
app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  next();
});
//fffff
app.options("*", cors());

app.get("/", (req, res) => {
  res.send("Inicio");
});

app.use(require("./src/routes/productoRoutes"));
app.listen(process.env.PORT || 3005, () => {
  console.log("Servicio online");
});
